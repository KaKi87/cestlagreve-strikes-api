process.on('uncaughtException', err => console.error(err.stack));
process.on('unhandledRejection', err => console.error(err.stack));

const
	fastify = require('fastify')(),
	cestlagreve = require('cestlagreve-strikes');

fastify.get('/', async (request, reply) => reply.code(204).send());

fastify.get('/ics', async (req, res) => res.send(await cestlagreve.getIcal()));

fastify.get('/embed', (req, res) => res.redirect(301, 'https://open-web-calendar.hosted.quelltext.eu/calendar.html?url=https://cestlagreve.api.kaki87.net/ics'));

fastify
	.listen(3071, '0.0.0.0')
	.then(() => console.log('Server running'))
	.catch(console.error);
